#include "FastNoise.h"
#include <stdio.h>
#include <iostream>
#include <fstream>
using namespace std;

int main()
{
	ofstream fp;

	 /*************************************/
	/* Generación de la textura de forma */
   /*************************************/

	int w = 128, h = 128, d = 128;

	const int period_shape[] = { 5, 10, 20, 30 };
	const float freq_shape[] = { period_shape[0] / (float) w, period_shape[1] / (float) w,
						   		 period_shape[2] / (float) w, period_shape[3] / (float) w };

	FastNoise shape_noise;
	shape_noise.SetFractalLacunarity(2.0f);
	shape_noise.SetFractalOctaves(5);
	shape_noise.SetCellularReturnType(FastNoise::CellularReturnType::Distance);

	fp.open("ruido_forma.txt");

	// Cabecera de la textura
	fp << 3 << "\n"; // dimensión de la textura
	fp << 4 << "\n"; // número de canales de color (RGB o RGBA)
	fp << w << "\n"; // anchura
	fp << h << "\n"; // altura
	fp << d << "\n"; // profundidad

	// Datos de la textura
	float v_r, v_g, v_b, v_a = 0.0f;
	float w_v, p_v = 0.0f;
	for (int x = 0; x < w; x++)
	{
		for (int y = 0; y < h; y++)
		{
			for(int z = 0; z < d; z++)
			{
				// Obtenemos el ruido de perlin de frecuencia baja
				shape_noise.SetFrequency(freq_shape[0]);
				shape_noise.SetNoiseType(FastNoise::PerlinFractal);
				p_v = shape_noise.GetPeriodicNoise((float) x, (float) y, (float) z, period_shape[0], period_shape[0], period_shape[0]);

				// Obtenemos el ruido worley de frecuencias crecientes
				shape_noise.SetNoiseType(FastNoise::CellularFractal);
				w_v = shape_noise.GetPeriodicNoise((float) x, (float) y, (float) z, period_shape[0], period_shape[0], period_shape[0]);
				shape_noise.SetFrequency(freq_shape[1]);
				v_g = shape_noise.GetPeriodicNoise((float) x, (float) y, (float) z, period_shape[1], period_shape[1], period_shape[1]);
				shape_noise.SetFrequency(freq_shape[2]);
				v_b = shape_noise.GetPeriodicNoise((float) x, (float) y, (float) z, period_shape[2], period_shape[2], period_shape[2]);
				shape_noise.SetFrequency(freq_shape[3]);
				v_a = shape_noise.GetPeriodicNoise((float) x, (float) y, (float) z, period_shape[3], period_shape[3], period_shape[2]);



				// Invertimos el valor para el ruido worley
				w_v = (1.0f - w_v);
				p_v = ((p_v + 1.0f) * 0.5f);	// El ruido perlin esta en el rango [-1.0, 1.0], hay que pasarlo a [0.0, 1.0]
				v_g = (1.0f - v_g);
				v_b = (1.0f - v_b); 
				v_a = (1.0f - v_a);

				// Calculamos el ruido Perlin-Worley mezclando los dos ruidos
				v_r = (p_v+w_v)/2.0f;

				// Añadimos los valores a la textura
				fp << v_r << "\n";
				fp << v_g << "\n";
				fp << v_b << "\n";
				fp << v_a << "\n";
			}

		}
	}

	fp.close();

	 /***************************************/
	/* Generación de la textura de detalle */
   /***************************************/

	w = 32;
	h = 32;
	d = 32;

	const int period_detail[] = { 5, 10, 15 };
	const float freq_detail[] = { period_detail[0] / (float) w, period_detail[1] / (float) w,
					   		 period_detail[2] / (float) w};

	FastNoise detail_noise;
	detail_noise.SetNoiseType(FastNoise::CellularFractal);
	detail_noise.SetFractalLacunarity(2.0f);
	detail_noise.SetFractalOctaves(5);
	detail_noise.SetCellularReturnType(FastNoise::CellularReturnType::Distance);

	fp.open("ruido_detalle.txt");

	// Cabecera de la textura
	fp << 3 << "\n"; // dimensión de la textura
	fp << 4 << "\n"; // número de canales de color (RGB o RGBA)
	fp << w << "\n"; // anchura
	fp << h << "\n"; // altura
	fp << d << "\n"; // profundidad

	// Datos de la textura
	v_r, v_g, v_b, v_a = 0.0f;
	for (int x = 0; x < w; x++)
	{
		for (int y = 0; y < h; y++)
		{
			for(int z = 0; z < d; z++)
			{
				// Obtenemos el ruido worley de frecuencias crecientes
				detail_noise.SetFrequency(freq_detail[0]);
				v_r = detail_noise.GetPeriodicNoise((float) x, (float) y, (float) z, period_detail[0], period_detail[0], period_detail[0]);
				detail_noise.SetFrequency(freq_detail[1]);
				v_g = detail_noise.GetPeriodicNoise((float) x, (float) y, (float) z, period_detail[1], period_detail[1], period_detail[1]);
				detail_noise.SetFrequency(freq_detail[2]);
				v_b = detail_noise.GetPeriodicNoise((float) x, (float) y, (float) z, period_detail[2], period_detail[2], period_detail[2]);


				// Invertimos el valor del ruido worley
				v_r = (1.0f - v_r);
				v_g = (1.0f - v_g);
				v_b = (1.0f - v_b);

				// Añadimos los valores a la textura
				fp << v_r << "\n";
				fp << v_g << "\n";
				fp << v_b << "\n";
				fp << v_a << "\n";
			}

		}
	}

	fp.close();

	return 0;
}
